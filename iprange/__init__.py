import argparse
from .ipaddress import IPv4Address


__all__ = 'increase_ip', 'generate_range', 'generate_subnet', 'main', 'IPv4Address'


def increase_ip(ip):
  return IPv4Address(ip) + IPv4Address(_int_addr=1)
  

def generate_range(ip_start: str, ip_end: str):
    start = IPv4Address(ip_start)
    end = IPv4Address(ip_end)
    if end.intaddr < start.intaddr: 
        raise ValueError('The end ip address must be '
                         'bigger or equal to the start address')
    for i in range(start.intaddr, end.intaddr + 1):
        yield IPv4Address(_int_addr=i)


def generate_subnet(ip_network:str, cidr:int):

    network = IPv4Address(ip_network)
    if not network.isnetwork(cidr):
        raise ValueError('network must be a network address')
    
    for i in range((0xFFFFFFFF >> cidr) + 1): # for the subnet incl. the broadcast addr
        yield IPv4Address(_int_addr=network.intaddr + i)


def main():
    pars = argparse.ArgumentParser()
    pars.add_argument('ip_start')
    pars.add_argument('--end')
    pars.add_argument('--subnet', type=int)

    args = pars.parse_args()

    if args.end:
        for i in generate_range(args.ip_start, args.end):
            print(i)
    elif args.subnet:
        for i in generate_subnet(args.ip_start, args.subnet):
            print(i)

